"""Configuration file for the Sphinx documentation builder."""

# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

project = "Python modules"
copyright = "2020, Berthold Höllmann <berhoel@gmail.com>"

# -- theme selection -------------------------------------------------

from berhoel.sphinx_settings import *  # isort:skip
from berhoel import sphinx_settings  # isort:skip

extensions = [i for i in extensions if "sitemap" not in i]

html_theme = "berhoel_sphinx_theme"
html_theme_path = sphinx_settings.get_html_theme_path()

html_static_path = ["_static"]

html_theme_options = {
    "navbar_links": [("GitLab", "https://gitlab.com/berhoel/python", True)],
    # Render the current pages TOC in the navbar. (Default: true)
    "navbar_pagenav": False,
}
